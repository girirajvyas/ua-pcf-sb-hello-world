# Hello world Spring boot application deployed on PCF #

Note: Pivotal is owned by VMware now


Commands  
cd D:\Sources\UA\ua-pcf-sb-hello-world  
cf login -a https://api.run.pivotal.io  
  - provide credentials to log in  

###
cf push
Error:
"Pushing app  to org springboot-helloworld / space development as girirajvyas21@gmail.com...
Incorrect Usage: The push command requires an app name. The app name can be supplied as an argument or with a manifest.yml file.
FAILED"
###

###
cf push test-environment -p target\ua-pcf-sb-hello-world-0.0.1-SNAPSHOT.jar
Pushing app test-environment to org springboot-helloworld / space development as girirajvyas21@gmail.com...
Getting app info...
The app cannot be mapped to route test-environment.cfapps.io because the route exists in a different space.
FAILED
###

###
cf push ua-pcf-sb-hello-world -p target\ua-pcf-sb-hello-world-0.0.1-SNAPSHOT.jar

###

###
D:\Sources\UA\ua-pcf-sb-hello-world>cf push ua-pcf-sb-hello-world -p target\ua-pcf-sb-hello-world-0.0.1-SNAPSHOT.jar
Pushing app ua-pcf-sb-hello-world to org springboot-helloworld / space development as girirajvyas21@gmail.com...
Getting app info...
Creating app with these attributes...
+ name:       ua-pcf-sb-hello-world
  path:       D:\Sources\UA\ua-pcf-sb-hello-world\target\ua-pcf-sb-hello-world-0.0.1-SNAPSHOT.jar
  routes:
+   ua-pcf-sb-hello-world.cfapps.io

Creating app ua-pcf-sb-hello-world...
Mapping routes...
Comparing local files to remote cache...
Packaging files to upload...
Uploading files...
 290.34 KiB / 290.34 KiB [=====================================================================================] 100.00% 2s

Waiting for API to complete processing files...

Staging app and tracing logs...
   Downloading web_config_transform_buildpack...
   Downloading staticfile_buildpack...
   Downloading java_buildpack...
   Downloading ruby_buildpack...
   Downloading dotnet_core_buildpack...
   Downloaded java_buildpack
   Downloading nodejs_buildpack...
   Downloaded staticfile_buildpack
   Downloading go_buildpack...
   Downloaded nodejs_buildpack
   Downloading python_buildpack...
   Downloaded ruby_buildpack
   Downloading php_buildpack...
   Downloaded web_config_transform_buildpack
   Downloading binary_buildpack...
   Downloaded dotnet_core_buildpack
   Downloading dotnet_core_buildpack_beta...
   Downloaded dotnet_core_buildpack_beta
   Downloaded go_buildpack
   Downloaded binary_buildpack
   Downloaded python_buildpack
   Downloaded php_buildpack
   Cell 9b74a5b1-9af9-4715-a57a-bd28ad7e7f1b creating container for instance 6a2d9fc8-12e3-4e48-b410-1eb28d52264a
   Cell 9b74a5b1-9af9-4715-a57a-bd28ad7e7f1b successfully created container for instance 6a2d9fc8-12e3-4e48-b410-1eb28d52264a
   Downloading app package...
   Downloaded app package (15M)
   -----> Java Buildpack v4.26 (offline) | https://github.com/cloudfoundry/java-buildpack.git#e06e00b
   -----> Downloading Jvmkill Agent 1.16.0_RELEASE from https://java-buildpack.cloudfoundry.org/jvmkill/bionic/x86_64/jvmkill-1.16.0-RELEASE.so (found in cache)
   -----> Downloading Open Jdk JRE 1.8.0_232 from https://java-buildpack.cloudfoundry.org/openjdk/bionic/x86_64/openjdk-jre-1.8.0_232-bionic.tar.gz (found in cache)
          Expanding Open Jdk JRE to .java-buildpack/open_jdk_jre (2.0s)
          JVM DNS caching disabled in lieu of BOSH DNS caching
   -----> Downloading Open JDK Like Memory Calculator 3.13.0_RELEASE from https://java-buildpack.cloudfoundry.org/memory-calculator/bionic/x86_64/memory-calculator-3.13.0-RELEASE.tar.gz (found in cache)
          Loaded Classes: 11884, Threads: 250
   -----> Downloading Client Certificate Mapper 1.11.0_RELEASE from https://java-buildpack.cloudfoundry.org/client-certificate-mapper/client-certificate-mapper-1.11.0-RELEASE.jar (found in cache)
   -----> Downloading Container Security Provider 1.16.0_RELEASE from https://java-buildpack.cloudfoundry.org/container-security-provider/container-security-provider-1.16.0-RELEASE.jar (found in cache)
   -----> Downloading Spring Auto Reconfiguration 2.11.0_RELEASE from https://java-buildpack.cloudfoundry.org/auto-reconfiguration/auto-reconfiguration-2.11.0-RELEASE.jar (found in cache)
   Exit status 0
   Uploading droplet, build artifacts cache...
   Uploading droplet...
   Uploading build artifacts cache...
   Uploaded build artifacts cache (129B)
   Uploaded droplet (57.2M)
   Uploading complete
   Cell 9b74a5b1-9af9-4715-a57a-bd28ad7e7f1b stopping instance 6a2d9fc8-12e3-4e48-b410-1eb28d52264a
   Cell 9b74a5b1-9af9-4715-a57a-bd28ad7e7f1b destroying container for instance 6a2d9fc8-12e3-4e48-b410-1eb28d52264a
   Cell 9b74a5b1-9af9-4715-a57a-bd28ad7e7f1b successfully destroyed container for instance 6a2d9fc8-12e3-4e48-b410-1eb28d52264a

Waiting for app to start...

name:              ua-pcf-sb-hello-world
requested state:   started
routes:            ua-pcf-sb-hello-world.cfapps.io
last uploaded:     Sun 22 Mar 21:10:49 IST 2020
stack:             cflinuxfs3
buildpacks:        client-certificate-mapper=1.11.0_RELEASE container-security-provider=1.16.0_RELEASE
                   java-buildpack=v4.26-offline-https://github.com/cloudfoundry/java-buildpack.git#e06e00b java-main
                   java-opts java-security jvmkill-agent=1.16.0_RELEASE open-jdk...

type:            web
instances:       1/1
memory usage:    1024M
start command:   JAVA_OPTS="-agentpath:$PWD/.java-buildpack/open_jdk_jre/bin/jvmkill-1.16.0_RELEASE=printHeapHistogram=1
                 -Djava.io.tmpdir=$TMPDIR -XX:ActiveProcessorCount=$(nproc)
                 -Djava.ext.dirs=$PWD/.java-buildpack/container_security_provider:$PWD/.java-buildpack/open_jdk_jre/lib/ext
                 -Djava.security.properties=$PWD/.java-buildpack/java_security/java.security $JAVA_OPTS" &&
                 CALCULATED_MEMORY=$($PWD/.java-buildpack/open_jdk_jre/bin/java-buildpack-memory-calculator-3.13.0_RELEASE
                 -totMemory=$MEMORY_LIMIT -loadedClasses=12665 -poolType=metaspace -stackThreads=250
                 -vmOptions="$JAVA_OPTS") && echo JVM Memory Configuration: $CALCULATED_MEMORY && JAVA_OPTS="$JAVA_OPTS
                 $CALCULATED_MEMORY" && MALLOC_ARENA_MAX=2 SERVER_PORT=$PORT eval exec
                 $PWD/.java-buildpack/open_jdk_jre/bin/java $JAVA_OPTS -cp $PWD/.
                 org.springframework.boot.loader.JarLauncher
     state     since                  cpu    memory         disk           details
#0   running   2020-03-22T15:41:10Z   0.0%   149.4M of 1G   125.7M of 1G


D:\Sources\UA\ua-pcf-sb-hello-world>

###

###
https://ua-pcf-sb-hello-world.cfapps.io/hello
###

```cmd
D:\Sources\UA\ua-pcf-sb-hello-world>cf login -a https://api.run.pivotal.io
API endpoint: https://api.run.pivotal.io

Email: girirajvyas21@gmail.com

Password:
Authenticating...
OK

Targeted org springboot-helloworld

Targeted space development



API endpoint:   https://api.run.pivotal.io (API version: 3.81.0)
User:           girirajvyas21@gmail.com
Org:            springboot-helloworld
Space:          development

D:\Sources\UA\ua-pcf-sb-hello-world>

```


References:
follow me link: https://www.javainuse.com/pcf/pcf-intro
official 15 minutes startup guide: https://tanzu.vmware.com/tutorials/getting-started/introduction


