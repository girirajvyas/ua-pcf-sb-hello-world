package com.epam.ua;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UaPcfSbHelloWorldApplication {

	public static void main(String[] args) {
		SpringApplication.run(UaPcfSbHelloWorldApplication.class, args);
	}

}
